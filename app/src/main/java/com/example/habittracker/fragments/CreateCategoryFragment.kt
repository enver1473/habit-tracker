package com.example.habittracker.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.habittracker.R
import com.example.habittracker.databinding.FragmentCreateCategoryBinding


class CreateCategoryFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentCreateCategoryBinding>(inflater, R.layout.fragment_create_category, container, false)
        return binding.root
    }

}
