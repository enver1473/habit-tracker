package com.example.habittracker.database

import com.example.habittracker.database.entities.Activity
import com.example.habittracker.database.entities.ActivityInstance
import com.example.habittracker.database.entities.Category

class DBData {

    val categories: List<Category> = listOf(
        Category(
            1, "Medication", "amount", "mg", true, null, System.currentTimeMillis()
        ),
        Category(
            2, "Workout", "time", null, true, null, System.currentTimeMillis()
        ),
        Category(
            3, "Study", "time", null, true, null, System.currentTimeMillis()
        ),
        Category(
            4, "Nutrition", "amount", "kcal", true, null, System.currentTimeMillis()
        ),
        Category(
            5, "Hygiene", "incremental", "times", true, 1.0, System.currentTimeMillis()
        ),
        Category(
            6, "Meditation", "time", null, true, null, System.currentTimeMillis()
        )
    )

    val activities: List<Activity> = listOf(
        Activity(
            1, "Running", 2, System.currentTimeMillis()
        ),
        Activity(
            2, "Running", 2, System.currentTimeMillis()
        ),/*
        Activity(
            3, "vitamins", 1, System.currentTimeMillis()
        ),
        Activity(
            4, "programming", 3, System.currentTimeMillis()
        ),
        Activity(
            5, "washing hands", 5, System.currentTimeMillis()
        ), Activity(
            6, "Running", 2, System.currentTimeMillis()
        ),
        Activity(
            7, "Running", 2, System.currentTimeMillis()
        ),
        Activity(
            8, "vitamins", 1, System.currentTimeMillis()
        ),
        Activity(
            9, "programming", 3, System.currentTimeMillis()
        ),
        Activity(
            10, "washing hands", 5, System.currentTimeMillis()
        ),*/
        Activity(
            11, "Running", 2, System.currentTimeMillis()
        ),
        Activity(
            12, "Running", 2, System.currentTimeMillis()
        ),
        Activity(
            13, "Running", 2, System.currentTimeMillis()
        ),
        Activity(
            14, "Running", 2, System.currentTimeMillis()
        )
    )

    val activityInstances: List<ActivityInstance> = listOf(
        ActivityInstance(
            1, 1, null, System.currentTimeMillis() + 30000, System.currentTimeMillis() + 3629230
        )
    )
}

/*

lijekovi kolicinski miligrami
vjezbanje vremensko,
ucenje vremensko,
ishrana kolicinsko,
higijena inkrementalno
meditacija vremensko

*/
