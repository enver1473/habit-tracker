package com.example.habittracker.adapters


import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.habittracker.R
import com.example.habittracker.database.entities.Activity
import com.example.habittracker.database.entities.FullActivity
import com.example.habittracker.screens.ActivityFragment
import com.example.habittracker.screens.ActivityViewModel
import kotlin.math.log

class ActivityIncrementalAdapter(activityList: List<FullActivity>, viewholder: ActivityViewModel) :
    RecyclerView.Adapter<ActivityIncrementalAdapter.ActivityViewHolder>() {
    val activityList: List<FullActivity>
    val viewholder: ActivityViewModel
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ActivityViewHolder {
        Log.d("l", viewType.toString())
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.incremental_item, parent, false)
        return ActivityViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: ActivityViewHolder,
        position: Int
    ) {
        val activity: FullActivity = activityList[position]
        Log.d("tag", activity.toString())

        holder.activityName.text =
            activity.activity.name + " " + activity.activityInstances[0].value + " times"

        holder.buttonIncrease.setOnClickListener {
            viewholder.increaseValue(
                activity.activityInstances[0].instanceId,
                activity.activityInstances[0].activityId,
                activity.activityInstances[0].value
            )
        }
        holder.buttonDecrease.setOnClickListener {
            viewholder.decreaseValue(
                activity.activityInstances[0].instanceId,
                activity.activityInstances[0].activityId,
                activity.activityInstances[0].value
            )
        }

    }

    override fun getItemCount(): Int {
        return activityList.size
    }

    class ActivityViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var activityName: TextView
        var buttonIncrease: View
        var buttonDecrease: View

        init {
            activityName = itemView.findViewById(R.id.incrementalName)
            buttonIncrease = itemView.findViewById(R.id.increase)
            buttonDecrease = itemView.findViewById(R.id.decrease)

        }
    }

    init {
        this.activityList = activityList
        this.viewholder = viewholder
    }
}