package com.example.habittracker.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.habittracker.R
import com.example.habittracker.database.entities.Category
import com.example.habittracker.screens.HomeFragmentDirections
import java.util.*

 class CategoryAdapter(categoryList: ArrayList<Category>) :
    RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {
    val categoryList: ArrayList<Category>
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CategoryViewHolder {
        Log.d("tip",parent.toString())
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.category_item, parent, false)
        return CategoryViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: CategoryViewHolder,
        position: Int
    ) {
        val category: Category = categoryList[position]
        holder.categoryName.setText(category.name)
        holder.card.setOnClickListener(
            Navigation.createNavigateOnClickListener(HomeFragmentDirections.actionHomeFragmentToActivityFragment(category.categoryId, category.type, category.unitOfMeasure ?: ""))
        )


        when (category.name) {
            "Medication" -> holder.imageCategory.setImageResource(R.drawable.pharmacy)
            "Workout" -> holder.imageCategory.setImageResource(R.drawable.physical_education)
            "Study" -> holder.imageCategory.setImageResource(R.drawable.study)
            "Nutrition" -> holder.imageCategory.setImageResource(R.drawable.artichoke)
            "Hygiene" -> holder.imageCategory.setImageResource(R.drawable.service_water)
            "Meditation" -> holder.imageCategory.setImageResource(R.drawable.user_asleep)


        }

    }

    override fun getItemCount(): Int {
        return categoryList.size
    }

     class CategoryViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var categoryName: TextView
         var imageCategory : ImageView
         var card : CardView




        init {
            categoryName = itemView.findViewById(R.id.categoryName)
            imageCategory = itemView.findViewById(R.id.imageCategory)
            card = itemView.findViewById(R.id.cardCategory)

        }
    }

    init {
        this.categoryList = categoryList
    }
}