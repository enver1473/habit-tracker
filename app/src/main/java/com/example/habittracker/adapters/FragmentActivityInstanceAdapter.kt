package com.example.habittracker.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.habittracker.R
import com.example.habittracker.database.entities.ActivityInstance
import com.example.habittracker.database.entities.FullActivity
import java.text.DateFormat
import java.time.format.DateTimeFormatter
import java.util.*

class FragmentActivityInstanceAdapter(private val instanceList: List<ActivityInstance>) :
    RecyclerView.Adapter<FragmentActivityInstanceAdapter.ActivityViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ActivityViewHolder {
        Log.d("l", viewType.toString())
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.time_instance, parent, false)
        return ActivityViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: ActivityViewHolder,
        position: Int
    ) {
        val instance = instanceList[position]

        val startTimeText = DateFormat.getDateTimeInstance().format(Date(instance.startTimeMillis)).toString()
        val endTimeText = DateFormat.getDateTimeInstance().format(Date(instance.endTimeMillis)).toString()

        holder.startTime.text = "Started at: " + startTimeText
        holder.endTime.text = "Stopped at: " + endTimeText
    }

    override fun getItemCount(): Int {
        return instanceList.size
    }

    class ActivityViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var startTime: TextView = itemView.findViewById(R.id.start_time)
        var endTime: TextView = itemView.findViewById(R.id.end_time)
    }

}