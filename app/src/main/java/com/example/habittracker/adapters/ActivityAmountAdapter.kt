package com.example.habittracker.adapters


import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.lifecycle.LiveData
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.habittracker.R
import com.example.habittracker.database.entities.FullActivity
import com.example.habittracker.screens.ActivityFragmentDirections
import com.example.habittracker.screens.ActivityViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ActivityAmountAdapter(activityList: List<FullActivity>, private val viewModel: ActivityViewModel, private val categoryId: Long, private val categoryType: String, private val unitOfMeasure: String) :
    RecyclerView.Adapter<ActivityAmountAdapter.ActivityViewHolder>() {
    val activityList: List<FullActivity> = activityList
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ActivityViewHolder {
        Log.d("l", viewType.toString())
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.amount_item, parent, false)
        return ActivityViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: ActivityViewHolder,
        position: Int
    ) {
        val activity: FullActivity = activityList[position]
        holder.activityName.text = activity.activity.name + " " + "Amount : "
        holder.card.setOnClickListener(
            Navigation.createNavigateOnClickListener(
                ActivityFragmentDirections.actionActivityFragmentToActivityInstanceFragment(
                    activity.activity.activityId, categoryId, unitOfMeasure, categoryType
                )
            )
        )

        holder.addAmountFab.setOnClickListener {
            val value = holder.editTextNumberDecimal.text.toString()
            if (value == "") {
                return@setOnClickListener
            }
            viewModel.insertActivityInstance(activity.activity.activityId, value.toDouble())
        }

    }

    override fun getItemCount(): Int {
        return activityList.size
    }

    class ActivityViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var activityName: TextView = itemView.findViewById(R.id.amountitem)
        var card: CardView = itemView.findViewById(R.id.amount_card)
        var addAmountFab: FloatingActionButton = itemView.findViewById(R.id.add_amount)
        var editTextNumberDecimal: EditText = itemView.findViewById(R.id.editTextNumberDecimal)
    }

}