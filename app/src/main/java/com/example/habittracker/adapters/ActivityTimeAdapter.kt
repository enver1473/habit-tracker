package com.example.habittracker.adapters


import android.app.Application
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.lifecycle.LiveData
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.habittracker.R
import com.example.habittracker.database.entities.Activity
import com.example.habittracker.database.entities.FullActivity
import com.example.habittracker.screens.ActivityFragmentDirections
import com.example.habittracker.screens.ActivityInstanceViewModel
import com.example.habittracker.screens.ActivityViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ActivityTimeAdapter(
    private val activityList: List<FullActivity>,
    private val viewModel: ActivityViewModel
) :
    RecyclerView.Adapter<ActivityTimeAdapter.ActivityViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ActivityViewHolder {
        Log.d("l", viewType.toString())
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.time_item, parent, false)
        return ActivityViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: ActivityViewHolder,
        position: Int
    ) {
        val activity: FullActivity = activityList[position]
        holder.activityName.text = activity.activity.name

        holder.card.setOnClickListener(
            Navigation.createNavigateOnClickListener(
                ActivityFragmentDirections.actionActivityFragmentToActivityInstanceFragment(
                    activity.activity.activityId, 0, "", "time"
                )
            )
        )

        holder.startButton.setOnClickListener {
            Log.i("v", "kliknut")

            val lastInstance =  if (activity.activityInstances.isEmpty()) null else activity.activityInstances[activity.activityInstances.size - 1]

            if (lastInstance != null && lastInstance.startTimeMillis == lastInstance.endTimeMillis) {
                return@setOnClickListener
            }

            viewModel.insertActivityInstance(activity.activity.activityId)

            val toast = Toast.makeText(holder.activityName.context, "Activity started!", Toast.LENGTH_LONG)
            toast.show()
        }
        holder.stopButton.setOnClickListener {
            Log.i("v", "kliknut")

            if (activity.activityInstances.isEmpty()) {
                return@setOnClickListener
            }

            val lastInstance = activity.activityInstances[activity.activityInstances.size - 1]

            if(lastInstance.startTimeMillis != lastInstance.endTimeMillis) {
                return@setOnClickListener
            }
            lastInstance.endTimeMillis = System.currentTimeMillis()

            viewModel.updateActivityInstance(lastInstance)

            val toast = Toast.makeText(holder.activityName.context, "Activity stopped!", Toast.LENGTH_LONG)
            toast.show()
        }
    }

    override fun getItemCount(): Int {
        return activityList.size
    }

    class ActivityViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var activityName: TextView = itemView.findViewById(R.id.textAmount)
        var startButton: FloatingActionButton = itemView.findViewById(R.id.start)
        var stopButton: FloatingActionButton = itemView.findViewById(R.id.stop)
        var card: CardView = itemView.findViewById(R.id.time_card)
    }

}