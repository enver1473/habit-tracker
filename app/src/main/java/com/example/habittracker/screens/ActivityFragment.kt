package com.example.habittracker.screens

import android.app.Application
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.habittracker.R
import com.example.habittracker.adapters.ActivityAmountAdapter
import com.example.habittracker.adapters.ActivityIncrementalAdapter
import com.example.habittracker.adapters.ActivityTimeAdapter
import com.example.habittracker.databinding.ActivityFragmentBinding
import com.example.habittracker.dialog.ActivityDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ActivityFragment : Fragment() {

    companion object {
        fun newInstance() = ActivityFragment()
    }

    private lateinit var fab: FloatingActionButton
    private lateinit var appContext: Application

    private lateinit var viewModel: ActivityViewModel
    private lateinit var activityViewModelFactory: ActivityViewModelFactory

    private lateinit var binding: ActivityFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.activity_fragment, container, false)
        fab = binding.createActivity

        // get application context
        appContext = this.activity?.applicationContext as Application

        // get arguments passed from previous fragment
        val args = ActivityFragmentArgs.fromBundle(requireArguments())

        // initialize view model factory with additional arguments: context and category id
        activityViewModelFactory = ActivityViewModelFactory(appContext, args.id)

        // initialize view model using factory
        viewModel = ViewModelProvider(this, activityViewModelFactory).get(ActivityViewModel::class.java)

        // Log.i("v", viewModel.activities.toString())

        // set lifecycle owner to be this fragment
        binding.lifecycleOwner = this

        // setup observer for activities LiveData in order to update adapter any time it's list has been updated
        viewModel.activities.observe(viewLifecycleOwner, Observer { newActivities ->

            Log.i("v", newActivities.toString())

            // check which category type the parent category is and set corresponding adapter
            when (args.categoryType) {
                "time" -> binding.activityRecycleIncremental.also {
                    it.adapter = ActivityTimeAdapter(newActivities, viewModel)
                }
                "amount" -> binding.activityRecycleIncremental.also {
                    it.adapter = ActivityAmountAdapter(newActivities, viewModel, args.id, args.categoryType, args.unitOfMeasure)
                }
                else -> binding.activityRecycleIncremental.also {
                    it.adapter = ActivityIncrementalAdapter(newActivities, viewModel)
                }
            }
        })

        // click listener for fab to open a modal dialog
        fab.setOnClickListener(View.OnClickListener {

            val dialog = ActivityDialog(viewModel, args.categoryType)

            // The device is using a large layout, so show the fragment as a dialog
            activity?.supportFragmentManager?.let { it1 -> dialog.show(it1, "dialog") }
            Log.i("fablistener", dialog.toString())
        })

        return binding.root
    }
}