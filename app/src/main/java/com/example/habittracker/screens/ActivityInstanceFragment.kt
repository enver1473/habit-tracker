package com.example.habittracker.screens

import android.app.Application
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.habittracker.R
import com.example.habittracker.adapters.FragmentActivityInstanceAdapter
import com.example.habittracker.adapters.FragmentActivityInstanceAmountAdapter
import com.example.habittracker.databinding.FragmentActivityInstanceBinding

class ActivityInstanceFragment : Fragment() {

    private lateinit var appContext: Application

    private lateinit var viewModel: ActivityInstanceViewModel
    private lateinit var activityInstanceViewModelFactory: ActivityInstanceViewModelFactory

    private lateinit var binding: FragmentActivityInstanceBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_activity_instance, container, false)

        // get application context
        appContext = this.activity?.applicationContext as Application

        // get arguments passed from previous fragment
        val args = ActivityInstanceFragmentArgs.fromBundle(requireArguments())

        // initialize view model factory with additional arguments: context and category id
        activityInstanceViewModelFactory = ActivityInstanceViewModelFactory(appContext, args.activityId, args.categoryId, args.unitOfMeasure)

        Log.i("v", args.toString())

        // initialize view model using factory
        viewModel = ViewModelProvider(this, activityInstanceViewModelFactory).get(ActivityInstanceViewModel::class.java)

        // set lifecycle owner to be this fragment
        binding.lifecycleOwner = this

        viewModel.instances.observe(viewLifecycleOwner, Observer { newInstances ->
            Log.i("v", newInstances.toString())
            when (args.categoryType) {
                "time" ->
                    binding.activityInstanceRecycler.adapter = FragmentActivityInstanceAdapter(newInstances)
                else -> binding.activityInstanceRecycler.adapter = FragmentActivityInstanceAmountAdapter(newInstances, args.unitOfMeasure)
            }

        })

        return binding.root
    }
}