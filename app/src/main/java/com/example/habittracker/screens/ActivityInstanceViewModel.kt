package com.example.habittracker.screens

import android.app.Application
import android.util.Log
import androidx.lifecycle.ViewModel
import com.example.habittracker.database.HabitTrackerDB
import com.example.habittracker.database.entities.*

class ActivityInstanceViewModel(
    application: Application,
    val activityId: Long,
    val categoryId: Long,
    val unitOfMeasure: String
) : ViewModel() {

    private val db = HabitTrackerDB.getInstance(application).habitTrackerDAO()
    var instances = db.getInstancesByActivityId(activityId)

    init {

        Log.i("v", instances.toString())

    }

    fun insertActivityInstance() {
        db.insertActivityInstance(
            ActivityInstance(0, activityId, null)
        )
    }

    fun updateActivityInstance(activityInstance: ActivityInstance) {
        db.updateInstance(activityInstance)
    }
}