package com.example.habittracker.screens


import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ActivityInstanceViewModelFactory(
    private val appContext: Application,
    private val activityId: Long,
    private val categoryId: Long,
    private val unitOfMeasure: String
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ActivityInstanceViewModel::class.java)) {
            return ActivityInstanceViewModel(appContext, activityId, categoryId, unitOfMeasure) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}