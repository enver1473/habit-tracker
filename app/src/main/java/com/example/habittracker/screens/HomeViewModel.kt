package com.example.habittracker.screens

import android.util.Log
import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.example.habittracker.database.HabitTrackerDB
import com.example.habittracker.database.entities.*

class HomeViewModel(application: Application) : AndroidViewModel(application) {

    private val db = HabitTrackerDB.getInstance(application).habitTrackerDAO()
    var categories = db.getAllCategories()

    fun insertCategory(name: String, typec: String) {
        db.insertCategory(Category(0, name, typec, null, false, null, System.currentTimeMillis()))
    }
}