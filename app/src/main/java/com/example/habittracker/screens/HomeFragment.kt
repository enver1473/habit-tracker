package com.example.habittracker.screens

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.habittracker.R
import com.example.habittracker.adapters.CategoryAdapter
import com.example.habittracker.database.entities.Category
import com.example.habittracker.databinding.FragmentHomeBinding
import com.example.habittracker.dialog.CategoryDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.util.ArrayList

class HomeFragment : Fragment() {

    private lateinit var viewModel: HomeViewModel

    private lateinit var binding: FragmentHomeBinding

    private lateinit var fab: FloatingActionButton

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        fab = binding.createCategory

        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

        viewModel.categories.observe(viewLifecycleOwner, Observer { newCategories ->
            val adapter = CategoryAdapter(newCategories as ArrayList<Category>)
            val manager = GridLayoutManager(activity, 3)
            binding.categoryList.layoutManager = manager
            binding.categoryList.adapter = adapter

        })

        fab.setOnClickListener {
            val dialog = CategoryDialog(viewModel)

            // The device is using a large layout, so show the fragment as a dialog
            activity?.supportFragmentManager?.let { it1 -> dialog.show(it1, "dialog") }
            // Log.i("fablistener", dialog.toString())
        }

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.options_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.item1 -> {
                requireView().findNavController()
                    .navigate(R.id.action_homeFragment_to_createCategoryFragment)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}
