package com.example.habittracker.screens

import android.app.Application
import android.text.Editable
import android.util.Log
import androidx.lifecycle.ViewModel
import com.example.habittracker.database.HabitTrackerDB
import com.example.habittracker.database.entities.*

class ActivityViewModel(private val application: Application, private val categoryId: Long) : ViewModel() {

    private val db = HabitTrackerDB.getInstance(application).habitTrackerDAO()
    var activities = db.getActivitiesByCategoryId(categoryId)

    init {

        Log.i("v", activities.toString())

    }

    fun insertIncrementalActivity(activityName: String) {
        val insertedId = db.insertActivity(
            Activity(0, activityName, categoryId)
        )
        db.insertActivityInstance(ActivityInstance(0,insertedId,.0))
    }

    fun insertActivity(activityName: String) {
        db.insertActivity(
            Activity(0, activityName, categoryId)
        )
    }

    fun insertActivityInstance(activityId: Long, value: Double? = null) {
        db.insertActivityInstance(
            ActivityInstance(0, activityId, value)
        )
    }

    fun updateActivityInstance(activityInstance: ActivityInstance) {
        db.updateInstance(activityInstance)
    }

    fun increaseValue (id : Long,activityId: Long, value: Double?) {
        if (value != null) {
            db.updateInstance(ActivityInstance(id, activityId ,value + 1))
        }
    }

    fun decreaseValue (id : Long,activityId: Long, value: Double?) {

        if (value != null) {
            db.updateInstance(ActivityInstance(id, activityId,value - 1))
        }
    }


}